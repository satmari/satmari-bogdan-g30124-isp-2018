package g30124.bogdan.satmari.l4.ex1;

import g30124.bogdan.satmari.l4.ex1.Box;

public class Main {

    public static void main(String[] args) {
        Conveyor cb1 = new Conveyor();
        Box p1 = new Box(cb1, 0, 7);
        Box p2 = new Box(cb1, 10, 3);


        cb1.display();
        cb1.moveRight();
        cb1.display();
        cb1.moveRight();
        cb1.display();
        cb1.moveRight();
        cb1.display();
        cb1.moveRight();
        cb1.display();
        cb1.moveLeft();
        cb1.display();
        cb1.moveLeft();
        cb1.display();
        cb1.moveLeft();
        cb1.display();
        cb1.moveLeft();
        cb1.display();
        cb1.moveLeft();
        cb1.display();
        cb1.moveLeft();
        cb1.display();
        cb1.moveLeft();
        cb1.display();
    }
}