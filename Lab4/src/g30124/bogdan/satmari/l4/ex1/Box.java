package g30124.bogdan.satmari.l4.ex1;

import g30124.bogdan.satmari.l4.ex1.Conveyor;

public class Box {
    private int id;

    public Box(Conveyor target, int pos, int id){
        this.id = id;
        target.addPackage(this,pos);
    }

    public int getId() {
        return id;
    }

    public String toString(){
        return ""+id;
    }
}