package g30124.satmari.bogdan.l5.e3;

import static org.junit.Assert.assertEquals;

import java.util.Timer;
import java.util.TimerTask;

import org.junit.Test;

import g30124.satmari.bogdan.l5.e3.LightSensor;
import g30124.satmari.bogdan.l5.e3.TemperatureSensor;
public class testt {
	@Test
	public void shouldShowTemperature()
	{
		Controller c = new Controller();
    	Timer myTimer = new Timer();
    	TimerTask task = new TimerTask() {
    		 int secondPassed = 0;
    		public void run() {
    			TemperatureSensor ts = new TemperatureSensor();
    			LightSensor ls = new LightSensor();
    			secondPassed++;
    			System.out.println("Sec("+secondPassed+") Temperature="+ts.ReadValue()+"  Light="+ls.ReadValue());   
    			if (secondPassed == 5)
    		        myTimer.cancel();
    		}
    	};
    	myTimer.scheduleAtFixedRate(task,1000,1000);
	}
}
