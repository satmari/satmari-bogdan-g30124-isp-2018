package g30124.satmari.bogdan.l5.e3;

public class TemperatureSensor extends Sensor{
	Random rand = new Random();
	int temp = rand.nextInt();
	@Override
	public int ReadValue() {
		if(temp>0)
		return temp%100;
		return temp%100 *(-1);
	}
}
