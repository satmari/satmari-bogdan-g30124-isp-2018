package g30124.satmari.bogdan.l5.e3;

public abstract class Sensor {
private String location;

abstract int ReadValue();

public String getLocation()
{
	return location;
}
}
