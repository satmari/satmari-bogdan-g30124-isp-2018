package g30124.satmari.bogdan.l5.e3;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class Controller {

	/*
    private ArrayList<Sensor> sensors = new ArrayList<Sensor>();
    
    public void addStudent(Sensor s){
        sensors.add(s);
    } 
	*/

public static void control()
{

	Timer myTimer = new Timer();
	TimerTask task = new TimerTask() {
		 int secondPassed = 0;
		public void run() {
			TemperatureSensor ts = new TemperatureSensor();
			LightSensor ls = new LightSensor();
			secondPassed++;
			System.out.println("Sec("+secondPassed+") Temperature="+ts.ReadValue()+"  Light="+ls.ReadValue());   
			if (secondPassed == 20)
		        myTimer.cancel();
		}
	};
	myTimer.scheduleAtFixedRate(task,1000,1000);
}
public static void main(String[] args)
{
	control();
}
}
