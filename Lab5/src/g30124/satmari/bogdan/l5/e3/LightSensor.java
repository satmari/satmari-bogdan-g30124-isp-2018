package g30124.satmari.bogdan.l5.e3;

public class LightSensor extends Sensor{
	Random rand = new Random();
	int light = rand.nextInt();
	@Override
	public int ReadValue() {
		if(light>0)
		return light%100;
		return light%100*(-1);
	}

}