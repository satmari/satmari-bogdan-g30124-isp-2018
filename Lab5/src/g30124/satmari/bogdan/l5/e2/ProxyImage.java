package g30124.satmari.bogdan.l5.e2;

public class ProxyImage implements Image{
	 
	   private RealImage realImage;
	   private String fileName;
	 
	   public ProxyImage(String fileName){
	      this.fileName = fileName;
	   }
	 
	  
		      
	   @Override
	   public void display() {
	      realImage.display();
	   }
	   
	  	@Override
		public void RotatedImage() {
		      realImage.RotatedImage();
		}
	  	
	  	public void Choose(int x)
	  	{
		      if(realImage == null)
			         realImage = new RealImage(fileName);
		      if( x == 2)
		          realImage.display();
		      else
		    	  realImage.RotatedImage();
	  	}


	}