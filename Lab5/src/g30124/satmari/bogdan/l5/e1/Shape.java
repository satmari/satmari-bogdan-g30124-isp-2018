package g30124.satmari.bogdan.l5.e1;

public abstract class Shape {
	private boolean filled;
	private String color;
Shape()
{
	filled = true;
	color = "red";
}
Shape(String color,boolean filled)
{
	this.color=color;
	this.filled=filled;
}
public void setColor(String color)
{
	this.color = color;
}
 public String getColor()
 {
	 return color;
 }
 public void setFilled(boolean filled)
 {
	 this.filled = filled;
 }
 public boolean isFilled()
 {
	 return filled;
 }
 
 public abstract double getArea();
 
 public abstract double getPerimetre();
 
 public String toString()
 {
	return " "; 
 }

 
}
