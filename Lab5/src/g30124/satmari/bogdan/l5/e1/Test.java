package g30124.satmari.bogdan.l5.e1;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class Test {
	private Shape q[] = new Shape[3];
	
	@Test
	public void testGetArea()
	{
		q[0]=new Circle();
		q[1]=new Rectangle();
		q[2]=new Square();
		assertEquals(q[0].getArea(),3.14,0.01);
		assertEquals(q[1].getArea(),2,0.01);
		assertEquals(q[2].getArea(),1,0.01);
	}
	@Test
	public void testGetPerimeter()
	{
		q[0]=new Circle();
		q[1]=new Rectangle();
		q[2]=new Square();
		assertEquals(q[0].getPerimetre(),6.28,0.01);
		assertEquals(q[1].getPerimetre(),6,0.01);
		assertEquals(q[2].getPerimetre(),4,0.01);

	}
}