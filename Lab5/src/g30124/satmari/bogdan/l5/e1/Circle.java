package g30124.satmari.bogdan.l5.e1;

public class Circle extends Shape{
double radius;
Circle()
{
	radius = 1;
}
Circle(double radius)
{
	this.radius = radius;
}
Circle(double radius, String color,boolean filled)
{
	this.radius = radius;
	setColor(color);
	setFilled(filled);
}
public void setRadius(int radius)
{
	this.radius = radius;
}
public double getRadius()
{
	return radius;
}
@Override
public double getArea()
{
	return 3.14*radius*radius;
}
@Override
public double getPerimetre()
{
	return 2*3.14*radius;
}
@Override
public String toString()
{
	return " 1"; 
}



}