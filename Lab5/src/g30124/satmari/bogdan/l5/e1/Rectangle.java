package g30124.satmari.bogdan.l5.e1;

public class Rectangle extends Shape{
	double width;
	double length;
	Rectangle()
	{
		this.width = 1;
		this.length = 2;
	}
	Rectangle(double width,double length)
	{
		this.width = width;
		this.length = length;
	}
	Rectangle(double width,double length,String color,boolean filled)
	{
		this.width = width;
		this.length = length;
		setColor(color);
		setFilled(filled);
	}
	public void setWidth(double width)
	{
		this.width=width;
	}
	public double getWidth()
	{
		return width;
	}
	public void setLength(double length)
	{
		this.length=length;
	}
	public double getLength()
	{
		return length;
	}
	@Override
	public double getArea()
	{
		return length*width;
	}
	@Override
	public double getPerimetre()
	{
		return 2*(length + width);
	}
	@Override
	public String toString()
	{
		return " 2";
	}
}

