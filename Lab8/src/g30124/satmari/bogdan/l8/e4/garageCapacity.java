package g30124.satmari.bogdan.l8.e4;

public class garageCapacity extends Exception {
    int nr;
    public garageCapacity(String msj, int nr) // daca s-a depasit capacitatea garajului
    {
        super(msj);
        this.nr=nr;
    }

    public int getNr() {
        return nr;
    }
}
