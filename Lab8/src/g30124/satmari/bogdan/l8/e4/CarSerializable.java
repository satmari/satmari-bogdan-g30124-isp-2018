package g30124.satmari.bogdan.l8.e4;

import java.io.*;
import java.lang.management.GarbageCollectorMXBean;

public class CarSerializable {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        CarFactory carFactory=new CarFactory();
        try{
        Car c1=carFactory.createCar("VW",1200);
        Car c2=carFactory.createCar("Audi",1000);
        Car c3=carFactory.createCar("Bmw",20);
        System.out.println("------");
        try{
        carFactory.sendToGarage(c1,"garageVW.dat");
        carFactory.sendToGarage(c3,"garageBMW.dat");
        carFactory.sendToGarage(c2,"garageAUDI.dat");}
        catch (garageCapacity g)
        {
            System.out.println(g.getMessage()+g.getNr());
        }
        System.out.println("------");
        Car c11=carFactory.takeFromGarage("garageVW.dat");
        Car c22=carFactory.takeFromGarage("garageAUDI.dat");
        Car c33=carFactory.takeFromGarage("garageBMW.dat");}
        catch (numberException e) {
            System.out.println("Exception:"+e.getMessage()+e.getNr());
        }
    }
    static class CarFactory  {
        static int nr=1,carsInside=0;
        Car createCar(String m,int pr) throws numberException
        {  if(nr>4)
            throw new numberException("Too many cars were build ",nr);
            Car c=new Car(m,pr);
            System.out.println(c+" was build");
            nr++;
            return c;
        }
        void sendToGarage(Car c,String garageName) throws IOException,garageCapacity{

            if(carsInside>4)
                throw new garageCapacity("Garajul e full",carsInside);
            carsInside++;
            ObjectOutputStream o=new ObjectOutputStream(new FileOutputStream(garageName));
            o.writeObject(c);
            System.out.println(c+" is in the garage");
        }
        Car takeFromGarage(String garageName) throws IOException,ClassNotFoundException{
            ObjectInputStream in=new ObjectInputStream(new FileInputStream(garageName));
            Car c=(Car)in.readObject();
            System.out.println(c+" is back on the road");
            carsInside--;
            return c;
        }
    }
    static class Car implements Serializable{
         String model;
         int price;

        public Car(String model, int price) {
            this.model = model;
            this.price = price;
        }

        @Override
        public String toString() {
            return
                    "Model: " + model  +
                    " price=" + price +"EUR";
        }
    }
}
