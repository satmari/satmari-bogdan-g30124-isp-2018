package g30124.satmari.bogdan.l2.e6;

import java.util.Scanner;
public class ex6 {
	

	static int fact(int n){
		int i,s=1;
		for(i=1; i<=n;i++)
			s*=i;
		return s;
	}
	

	static int fact1(int n){
		if (n==1) return 1;
		else return n*fact1(n-1);
	}
	
	public static void main(String[] args){
		int n;
		
		Scanner in = new Scanner(System.in);
		System.out.println(" n= ");
		n = in.nextInt();
		
		System.out.println(" n1!: "+fact(n));
		System.out.println(" n2!: "+fact1(n));
		in.close();
	}
}
