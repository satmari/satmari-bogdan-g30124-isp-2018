package g30124.satmari.bogdan.l3.e6;

class MyPoint {
	private int x;
	private int y;
	
	public MyPoint(int newX,int newY)
	{
		x = newX;
		y = newY;
	}
	
	public double distance(int x1,int y1)
	{
		double d;
		d = Math.sqrt((y1-y)*(y1-y) + (x1-x)*(x1-x));
		return d;
	}
	
	public double distance(MyPoint p)
	{
		double d;
		d = Math.sqrt((p.getY()-y)*(p.getY()-y) + (p.getX()-x)*(p.getX()-x));
		return d;
	}
	
	public void setXY(int newX, int newY)
	{
		x = newX;
		y = newY;
	}
	public int getX()
	{
		return x;
	}
	public int getY()
	{
		return y;
	}
	public String toString()
	{
		return "("+x+","+y+")";
	}
	
	
}