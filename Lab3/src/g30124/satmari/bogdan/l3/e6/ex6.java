package g30124.satmari.bogdan.l3.e6;

import java.util.*;

public class ex6 {
	
	public static void main(String[] args) {	

		MyPoint p1 = new MyPoint(0,0);
		System.out.println(p1.toString());
		int x, y;
		System.out.println("Dati coordonatele punctului");
		Scanner in = new Scanner(System.in);
		x = in.nextInt();
		y = in.nextInt();
		MyPoint p2 = new MyPoint(x,y);
		System.out.println("Distanta intre " + p2.toString() + " si " + p1.toString() + " este "+ p1.distance(x,y));
		//Metoda 1
		System.out.println("Distanta intre " + p2.toString() + " si " + p1.toString() + " este "+ p1.distance(p2));
		//Metoda 2

	}
}
