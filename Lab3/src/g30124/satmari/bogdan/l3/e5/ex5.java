package g30124.satmari.bogdan.l3.e5;

import becker.robots.*;

public class ex5 {

	public static void main(String[] args)
	{
	City Londra = new City();
	Thing Colet = new Thing(Londra,2,2);
	Robot John = new Robot(Londra,1,2,Direction.SOUTH);
	Wall b0 = new Wall(Londra,1,1,Direction.NORTH);
	Wall b1 = new Wall(Londra,1,2,Direction.NORTH);
	Wall b2 = new Wall(Londra,1,2,Direction.EAST);
	Wall b3 = new Wall(Londra,1,1,Direction.WEST);
	Wall b4 = new Wall(Londra,2,1,Direction.WEST);
	Wall b5 = new Wall(Londra,2,1,Direction.SOUTH);
	Wall b6 = new Wall(Londra,2,2,Direction.NORTH);
	John.turnLeft();
	John.turnLeft();
	John.turnLeft();
	John.move();
	John.turnLeft();
	John.move();
	John.turnLeft();
	John.move();
	John.pickThing();
	John.turnLeft();
	John.turnLeft();
	John.move();
	John.turnLeft();
	John.turnLeft();
	John.turnLeft();
	John.move();
	John.turnLeft();
	John.turnLeft();
	John.turnLeft();
	John.move();
	John.turnLeft();
	John.turnLeft();
	John.turnLeft();
	
	}
}
