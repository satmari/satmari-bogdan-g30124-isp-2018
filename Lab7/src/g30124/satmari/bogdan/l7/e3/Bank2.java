package g30124.satmari.bogdan.l7.e3;

import g30124.satmari.bogdan.l7.e1.BankAcount;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.TreeSet;

public class Bank2 {
    TreeSet<BankAcount> accounts =new TreeSet<>();
    public void addAccount(String owner,double balance)
    {
        BankAcount b=new BankAcount(owner,balance);
        accounts.add(b);
    }
    public void printAccounts()
    {
        for(BankAcount b:accounts)
            System.out.println(b.toString());
    }

    public void printAccounts(double minBalance,double maxBalance)
    {
        for(BankAcount b:accounts)
        {
            if(b.getBalance() > minBalance && b.getBalance() < maxBalance)
                System.out.println(b.toString());
        }
    }
    public BankAcount getAccount(String owner)
    {
        Iterator<BankAcount> iterator=accounts.iterator();
        while(iterator.hasNext())
        {
            BankAcount b= iterator.next();
            if(b.getName().equals(owner))
                return b;

        }
        return null;
    }

    public TreeSet<BankAcount> getAccounts() {
        return accounts;
    }
}
