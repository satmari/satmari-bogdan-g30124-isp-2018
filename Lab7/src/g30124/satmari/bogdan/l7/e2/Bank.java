package g30124.satmari.bogdan.l7.e2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

public class Bank {
ArrayList<BankAcount> a = new ArrayList<BankAcount>();	
	
public void addAcount(String owner,double balance)
{
	BankAcount b = new BankAcount(owner,balance); 
	a.add(b);
}

public void printAcounts()
{
	for(int i=0;i<a.size();i++)
	{
		BankAcount b=a.get(i);
		System.out.println("BankAcount(" + i + ")" +b.getName() + " " + b.getBalance());
	}
}

public void printAcounts(double minBalance,double maxBalance)
{
	for(int i=0;i<a.size();i++)
	{
		BankAcount b=a.get(i);
		if(b.getBalance() >= minBalance && b.getBalance()<=maxBalance)
		System.out.println("BankAcount(" + i + ")" +b.getName() + " " + b.getBalance());
	}
}

public BankAcount getAcount(String owner)
{
	for(int i=0;i<a.size();i++)
	{
		BankAcount b = a.get(i);
	if(b.getName()==owner)
	{
		return b;	
	}
	}
	System.out.println("This Acount doesn t exist");
	return null;
}

public void sortare()
{
	  Comparator<BankAcount> BankAcountComparator = new Comparator<BankAcount>()
			  {
		  		public int compare(BankAcount b1,BankAcount b2)
		  			{
		  				return b1.compareTo(b2);
		  			}
			  };
}

}


