package g30124.satmari.bogdan.l7.e2;

import java.util.Comparator;

public class BankAcount implements Comparable<BankAcount>{
	private String owner;
	private double balance;
	
	public BankAcount(String owner,double balance)
	{
		super();
		this.owner = owner;
		this.balance = balance;
	}
	
	public String getName()
	{
		return this.owner;
	}
	
	public double getBalance()
	{
		return this.balance;
	}
	
	public void withdraw(double amount)
	{
		if(balance >= amount)
		{
			balance = balance - amount;
		}
		else
		{
			System.out.println("Not enought money");
		}
	}
	
	
	public void deposit(double amount)
	{
		balance = balance + amount;
	}
	


	public int compareTo(BankAcount compareAcount) {
		 double compareAcount1 =((BankAcount) compareAcount).getBalance(); 
	        
	        //ascending order
	        return (int) (this.balance - compareAcount1);
	        
	        //descending order
	       // return (int) (compareAcount1 - this.balance);
	}
	
	
	public static Comparator<BankAcount> BankAcountComparator = new Comparator<BankAcount>() {

public int compare(BankAcount b1, BankAcount b2) {
//ascending order
return b1.compareTo(b2);

//descending order
//return fruitName2.compareTo(fruitName1);
}

};


	 }