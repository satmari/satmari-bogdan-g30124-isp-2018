package g30124.satmari.bogdan.l7.e1;

public class BankAcount {
	private String owner;
	private double balance;
	
	public BankAcount(String owner,double balance)
	{
		this.owner = owner;
		this.balance = balance;
	}
	
	public String getName()
	{
		return this.owner;
	}
	
	public double getBalance()
	{
		return this.balance;
	}
	
	public void withdraw(double amount)
	{
		if(balance >= amount)
		{
			balance = balance - amount;
		}
		else
		{
			System.out.println("Not enought money");
		}
	}
	
	public void deposit(double amount)
	{
		balance = balance + amount;
	}

	public boolean equals(BankAcount x)
		{
		if(x.getName() != this.owner)
			return false;
		if(x.getBalance() != this.balance)
			return false;
		
		return true;
		}

  public int hashCode() {
      return (int)(balance + owner.hashCode());
  }
	
	public static void Main(String[] args)
	{
		//ArrayList<BankAcount> a = new ArrayList<BankAcount>();
		BankAcount m = new BankAcount("Alex",100);
		BankAcount n = new BankAcount("Alex",100);
		BankAcount p = new BankAcount("Alin",100);
		
		System.out.println(p.equals(n));
	}
}