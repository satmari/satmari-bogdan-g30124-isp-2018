package g30124.satmari.bogdan.l7.e4;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

public class Dictionary {
    HashMap dictionar=new HashMap();
    public void addWord(Word word,Definition definition)
    {
        if(dictionar.containsKey(word))
            System.out.println("Cuvantul exista,modific def");
        else
            System.out.println("adaug cuvant");
        dictionar.put(word,definition);
    }

    public Definition getDefinition(Word word)
    {
        System.out.println(dictionar.containsKey(word));
        return (Definition) dictionar.get(word);
    }

    public void getAllWords()
    {

        dictionar.forEach((key,value)-> System.out.println(key));
    }
    public void getAllDefinitions()
    {
        dictionar.forEach((key, value) -> System.out.println( value));
    }


    @Override
    public String toString() {
        return "Dictionary{" +
                "dictionar=" + dictionar +
                '}';
    }
}