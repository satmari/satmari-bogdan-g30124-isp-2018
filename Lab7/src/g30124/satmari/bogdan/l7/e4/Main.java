package g30124.satmari.bogdan.l7.e4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        Dictionary dictionary=new Dictionary();
        char choice;
        Word word;
        Definition definition;
        String linie;
        BufferedReader fluxIn=new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("---MENIU---");
            System.out.println("a- Adauga cuvant");
            System.out.println("c- Cauta cuvat");
            System.out.println("l-Afiseaza cuvinte");
            System.out.println("e-iesire");
            System.out.println("d- Toate def");
            System.out.println("t-Toate cuvintele");
            linie=fluxIn.readLine();
            choice=linie.charAt(0);
            switch (choice)
            {
                case 'a': case 'A':
                System.out.println("Introduceti cuvantul");
                linie=fluxIn.readLine();
                if(linie.length()>1)
                {  word=new Word(linie);
                    System.out.println("introduceti def:");
                    linie =fluxIn.readLine();
                    definition=new Definition(linie);
                    dictionary.addWord(word,definition);
                } break;
                case 'd' : case 'D':
                    dictionary.getAllDefinitions(); break;

                case 't': case 'T':
                    dictionary.getAllWords();
                case 'c': case 'C':
                System.out.println("Cuvantul cautat");
                linie=fluxIn.readLine();
                if(linie.length()>1)
                {
                    word=new Word(linie);
                    Definition cauta=dictionary.getDefinition(word);
                    if(cauta !=null)
                        System.out.println("Definitia:"+cauta);
                    else
                        System.out.println("nu exista");
                }break;
                case 'l': case 'L':
                System.out.println("afiseaza:");
                dictionary.getAllWords();
                break;


            }
        }while ( choice!='e' && choice!='E');
    }
}