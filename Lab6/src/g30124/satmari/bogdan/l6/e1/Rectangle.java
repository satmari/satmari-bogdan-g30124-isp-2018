package g30124.satmari.bogdan.l6.e1;


import java.awt.*;

public class Rectangle extends Shape{

    private int length;

    public Rectangle(Color color, int length,int x,int y,String a,boolean fill) {
        super(color,x,y,a,fill);
        this.length = length;
    }
    
    public int getLength()
    {
    	return this.length;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(getX(), getY(), getLength(), getLength()+10);
        if(isFill()==true)
        {
        	g.fillRect(getX(), getY(), getLength(), getLength()+10);
        }
    }
}