package g30124.satmari.bogdan.l6.e1;

import java.awt.*;

public abstract class Shape {

    private Color color;
    private int x;
    private int y;
    private String id;
    private boolean fill;
    
    public void setX(int x)
    {
    	this.x = x;
    }
    
    public void setY(int y)
    {
    	this.y = y;
    }
    
    public int getX()
    {
    	return this.x;
    }
    
    public int getY()
    {
    	return this.y;
    }
    
    public String getId()
    {
    	return this.id;
    }
    
    public boolean isFill()
    {
    	return this.fill;
    }

    public Shape(Color color,int x,int y,String id,boolean fill) {
        this.color = color;
        this.y = y;
        this.x = x;
        this.id = id;
        this.fill=fill;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public abstract void draw(Graphics g);
}
