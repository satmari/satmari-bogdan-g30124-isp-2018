package g30124.satmari.bogdan.l6.e3;

import java.awt.Color;
import java.awt.Graphics;

public class Brick {
	private int l1;
	private int l2;
	private int x;
	private int y;
	
	public Brick(int a,int b,int x,int y)
	{
		this.l1=a;
		this.l2=b;
		this.x=x;
		this.y=y;
	}
	

	public int getHigh()
	{
		return this.l1;
	}
	
	public int getWidth()
	{
		return this.l2;
	}
	
	public int getX()
	{
		return this.x;
	}
	
	public int getY()
	{
		return this.y;
	}
	
	public void draw(Graphics g) {
        g.setColor(Color.RED);
        g.drawRect(getX(), getY(),getHigh(),getWidth());
        	g.fillRect(getX(), getY(), getHigh(),getWidth());
	}
		
	}
