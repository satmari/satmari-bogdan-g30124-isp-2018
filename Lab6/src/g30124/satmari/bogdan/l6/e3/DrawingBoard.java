package g30124.satmari.bogdan.l6.e3;

import java.awt.Graphics;

import javax.swing.*;

public class DrawingBoard extends JFrame {
	
	
	Brick[] bricks = new Brick[100];
	
	public DrawingBoard()
	{
		super();
		this.setTitle("Drawing board example");
		this.setSize(300,500);
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.setVisible(true);
	}

    public void addBrick(Brick b){
        for(int i=0;i<bricks.length;i++){
            if(bricks[i]==null){
                bricks[i] = b;
                break;
            }
        }
        this.repaint();
    }
    
    public void paint(Graphics g){
        for(int i=0;i<bricks.length;i++){
            if(bricks[i]!=null)
                bricks[i].draw(g);
        }
    }
    

}