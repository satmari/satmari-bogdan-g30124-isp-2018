package g30124.satmari.bogdan.l6.e2;

import java.awt.Color;

/**
 * @author mihai.hulea
 */
public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED, 90,50,50,"a",true);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN, 100,50,50,"b",false);
        b1.addShape(s2);
        Shape s3 = new Rectangle(Color.BLUE,50,50,50,"c",false);
        b1.addShape(s3);
       // b1.deleteById("a");
    }
}