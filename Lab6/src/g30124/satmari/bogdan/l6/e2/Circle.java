package g30124.satmari.bogdan.l6.e2;

import java.awt.Color;
import java.awt.Graphics;

public class Circle implements Shape{

    private Color color;
    private int x;
    private int y;
    private String id;
    private boolean fill;
    private int radius;
	
    public Circle(Color color,int x,int y,int radius, String id,boolean fill)
    {
        this.color = color;
        this.y = y;
        this.x = x;
        this.id = id;
        this.fill=fill;
        this.radius=radius;
    }
    
	@Override
	public int getX() {
		return this.x;
	}

	@Override
	public int getY() {
		return this.y;
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public boolean isFill() {
		return this.fill;
	}

	@Override
	public void Shape() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Color getColor() {
		return this.color;
	}
	
	   public int getRadius() {
	        return this.radius;
	    }

	@Override
	public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        g.drawOval(getX(),getY(),getRadius(),getRadius());
        if(isFill()==true)
        {
        	g.fillOval(getX(),getY(),getRadius(),getRadius());
        
	}

}
}
