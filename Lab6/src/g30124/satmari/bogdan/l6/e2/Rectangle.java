package g30124.satmari.bogdan.l6.e2;

import java.awt.Color;
import java.awt.Graphics;

public class Rectangle implements Shape{

    private Color color;
    private int x;
    private int y;
    private String id;
    private boolean fill;
    private int length;
    
    public Rectangle(Color color,int x,int y,int length, String id,boolean fill)
    {
        this.color = color;
        this.y = y;
        this.x = x;
        this.id = id;
        this.fill=fill;
        this.length=length;
    }
	@Override
	public int getX() {
		return this.x;
	}

	@Override
	public int getY() {
		return this.y;
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public boolean isFill() {
		return this.fill;
	}

	@Override
	public void Shape() {
		// TODO Auto-generated method stub	
	}

	@Override
	public Color getColor() {
		return this.color;
	}

    public int getLength()
    {
    	return this.length;
    }
	
	@Override
	public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(50,50,length,length+10);
        if(isFill()==true)
        {
        	g.fillRect(getX(), getY(), getLength(), getLength()+10);
        
		
	}

}
}